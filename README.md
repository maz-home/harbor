# HARBOR

## Install harbor with helm chart

```bash
helm repo add harbor https://helm.goharbor.io
kubectl create ns harbor
helm upgrade  harbor harbor/harbor --namespace harbor -f values.yaml --install
```

## Deploy with argocd

```bash
helm repo add harbor https://helm.goharbor.io
kubectl apply -f app.yaml
```
